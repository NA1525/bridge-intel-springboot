package com.bridge.intel.bridgeintelspringboot.repository;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.bridge.intel.bridgeintelspringboot.model.ClockConfiguration;

@Repository
public class ClockRepository {
     
	@Autowired
	private MongoTemplate mongoTemplate;
	
	public void addClockConfiguration(ClockConfiguration clockConfiguration) {
		mongoTemplate.save(clockConfiguration);
	}
	
	public ClockConfiguration getClockConfiguration(String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query,ClockConfiguration.class ,"CLOCK_CONFIG");
	}
	
	public List<ClockConfiguration> getAllClockConfigurations() {
		return mongoTemplate.findAll(ClockConfiguration.class ,"CLOCK_CONFIG");
	}
}
