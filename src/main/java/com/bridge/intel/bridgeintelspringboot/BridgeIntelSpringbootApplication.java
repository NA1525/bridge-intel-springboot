package com.bridge.intel.bridgeintelspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BridgeIntelSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(BridgeIntelSpringbootApplication.class, args);
	}

}
