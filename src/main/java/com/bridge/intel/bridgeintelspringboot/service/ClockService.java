package com.bridge.intel.bridgeintelspringboot.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class ClockService {
	
	public Map<String , List<Boolean>> calculateTime(String time){
		Map<String , List<Boolean>> map = new HashMap<String, List<Boolean>>();
		
		String[] timePart  = time.split(":");
		
		int hours = Integer.parseInt(timePart[0]);
		int minutes = Integer.parseInt(timePart[1]);
		int seconds = Integer.parseInt(timePart[2]);
		
		List<Boolean> row1 = new ArrayList<Boolean>();
		if(seconds %2 == 0) {
			row1.add(true);
		}else {
			row1.add(false);	
		}
		map.put("1", row1);
		
		List<Boolean> row2 = new ArrayList<Boolean>();
		int rowCount = hours/5;
		
		for(int i = 0; i< rowCount ; i++) {
			row2.add(true);
		}
		
		for(int i = rowCount; i< 4 ; i++) {
			row2.add(false);
		}
		map.put("2", row2);
		
		
		List<Boolean> row3 = new ArrayList<Boolean>();
		int rowCountHours = hours%5;
		
		for(int i = 0; i< rowCountHours ; i++) {
			row3.add(true);
		}
		
		for(int i = rowCountHours; i< 4 ; i++) {
			row3.add(false);
		}
		map.put("3", row3);
	
		List<Boolean> row4 = new ArrayList<Boolean>();
		int rowCountMin = minutes/5;
		
		for(int i = 0; i< rowCountMin ; i++) {
			row4.add(true);
		}
		
		for(int i = rowCountMin; i< 11 ; i++) {
			row4.add(false);
		}
		map.put("4", row4);
		
		
		List<Boolean> row5 = new ArrayList<Boolean>();
		int rowCountOneMin = minutes%5;
		
		for(int i = 0; i< rowCountOneMin ; i++) {
			row5.add(true);
		}
		
		for(int i = rowCountOneMin; i< 4 ; i++) {
			row5.add(false);
		}
		map.put("5", row5);
		return map;
	}

}
