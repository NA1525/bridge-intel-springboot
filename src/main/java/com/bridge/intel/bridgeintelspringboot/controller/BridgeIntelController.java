package com.bridge.intel.bridgeintelspringboot.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bridge.intel.bridgeintelspringboot.model.Blub;
import com.bridge.intel.bridgeintelspringboot.model.ClockConfiguration;
import com.bridge.intel.bridgeintelspringboot.model.ClockIdentifier;
import com.bridge.intel.bridgeintelspringboot.repository.ClockRepository;
import com.bridge.intel.bridgeintelspringboot.service.ClockService;

import lombok.extern.slf4j.Slf4j;
import java.util.UUID;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/v1")
@Slf4j
public class BridgeIntelController {
	
	@Autowired
	ClockRepository repository;
	
	@Autowired
	ClockService clockService; 
	
	@PostMapping("/addClockConfiguration")
	public void addClockConfiguration(@RequestBody ClockConfiguration clockConfiguration) {
		ClockIdentifier clockIdentifier = clockConfiguration.getClockIdentifier();
		ClockIdentifier.builder().id(UUID.randomUUID().toString()).build();
		clockConfiguration.builder().clockIdentifier(clockIdentifier).build();
		repository.addClockConfiguration(clockConfiguration);
	}
	
	@GetMapping("/getClockConfiguration/{id}")
	public ResponseEntity<ClockConfiguration> getClockConfiguration(@PathVariable String id){
		Blub blub = Blub.builder().offColor("##123").onColor("##2323").status(true).build();
		List<Blub> list = new ArrayList<>();
		list.add(blub);
		Map<String, List<Blub>> map = new HashMap();
		map.put("123", list);
		ClockIdentifier clockIdentifier = ClockIdentifier.builder().id("12121").name("First Clock").build();
		ClockConfiguration clockConfiguration = ClockConfiguration.builder().clockIdentifier(clockIdentifier)
				.clockConfig(map).build();
		return new ResponseEntity<>(clockConfiguration,HttpStatus.OK); 
		//return new ResponseEntity<>(repository.getClockConfiguration(id),HttpStatus.OK);
	}
	
	@GetMapping("/getAllClockConfiguration")
	public ResponseEntity<List<ClockConfiguration>> getClockConfiguration(){
		return new ResponseEntity<>(repository.getAllClockConfigurations(),HttpStatus.OK);
	}
	
	@GetMapping("/getClock/{time}")
	public ResponseEntity<Map<String , List<Boolean>>> getClock(@PathVariable String time){
		return new ResponseEntity<>(clockService.calculateTime(time),HttpStatus.OK);
	}
	
	

}
