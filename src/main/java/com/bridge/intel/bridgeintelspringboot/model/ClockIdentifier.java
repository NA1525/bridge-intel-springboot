package com.bridge.intel.bridgeintelspringboot.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClockIdentifier {
	private String id;
	private String name;
}
