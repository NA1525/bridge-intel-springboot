package com.bridge.intel.bridgeintelspringboot.model;

import java.util.List;
import java.util.Map;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Document(collection ="CLOCK_CONFIG")
public class ClockConfiguration {
	private ClockIdentifier clockIdentifier;
	private Map<String , List<Blub>> clockConfig;
}