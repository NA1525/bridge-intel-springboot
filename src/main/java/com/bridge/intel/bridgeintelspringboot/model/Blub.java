package com.bridge.intel.bridgeintelspringboot.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Blub {
	private boolean status;
	private String onColor;
	private String offColor;
}